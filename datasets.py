"""
KITTI 2015 raw data as Pytorch Datasets

Sources:
    - Data fetched by the "raw dataset download script",
      taken from https://www.cvlibs.net/datasets/kitti/raw_data.php
    - Concept from: https://www.learnpytorch.io/04_pytorch_custom_datasets/

Tickets:
    - TODO: Add a stereo mode
    - TODO; Add some ground truth modes
    - TODO: What about timestamps and other meta info
    - TODO: Use decorators?

"""
import os
import pathlib
import logging

import numpy as np
from PIL import Image

import torch
from torch.utils.data import Dataset

from torchvision import transforms


class Kitti2015ImagePairs(Dataset):
    def __init__(
        self, target_dir: str, img_num: str, transform=None, suffix: str = "png"
    ) -> None:
        self.consecutive_images_paths = self.get_consecutive_images_paths(
            target_dir=target_dir, img_num=img_num, suffix=suffix
        )
        self.transform = transform

    def get_consecutive_images_paths(
        self, target_dir: str, img_num: str, suffix: str
    ) -> list:
        sequence_paths = list(
            pathlib.Path(target_dir).glob(f"*/*/image_{img_num}/data")
        )
        consecutive_images_paths = []
        for sequence_path in sequence_paths:
            sequence_image_paths = sorted(list(sequence_path.glob(f"*.{suffix}")))
            sequence_consecutive_images_paths = [
                (sequence_image_path, sequence_image_paths[i + 1])
                for i, sequence_image_path in enumerate(sequence_image_paths[:-1])
            ]
            consecutive_images_paths += sequence_consecutive_images_paths
        return consecutive_images_paths

    def __len__(self) -> int:
        """
        Returns the total number of samples.
        """
        return len(self.consecutive_images_paths)

    def __getitem__(self, index) -> torch.Tensor:
        """
        TODO: A Docstring here
        """
        # TODO: Check terminology source- & target image
        consecutive_images_path = self.consecutive_images_paths[index]
        source_image = Image.open(consecutive_images_path[0])
        target_image = Image.open(consecutive_images_path[1])

        if self.transform:
            return self.transform(source_image), self.transform(target_image)
        else:
            return source_image, target_image


if __name__ == "__main__":
    logging.basicConfig(format="%(asctime)s - %(message)s", level=logging.INFO)

    # Run some Tests
    raw_data_path = "data/KITTI_2015/raw_data"

    kitti_image_pairs_dataset = Kitti2015ImagePairs(
        target_dir=raw_data_path,
        img_num="02",
        transform=transforms.ToTensor(),
    )
    source_image, target_image = kitti_image_pairs_dataset[0]

    logging.info(f"len(kitti_image_pairs_dataset): {len(kitti_image_pairs_dataset)}")
    logging.info(f"source_image.shape: {source_image.shape}")
    logging.info(f"target_image.shape: {target_image.shape}")
