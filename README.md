# Structure From Motion Pytorch

## Wrap up

This Repo contains the first part of a PyTorch implementation of [Every Pixel Counts ++: Joint Learning of Geometry and Motion with 3D Holistic Understanding](https://arxiv.org/abs/1810.06125). It contains a

- A KITTI 2015 Dataset
- DispNet()
- PoseNet()
- A simple L1 loss
- Some useful photometric transformations for Sfm (Structure from Motion)
- A simple trainig script

There are many Tickets (`TODOs`) in the code.

## Setup

- `$ conda env create -f conda_env.yml`
- `$ conda activate sfm`
- `$ pre-commit install`


## References

- https://github.com/chenxuluo/EPC
- https://github.com/ClementPinard/SfmLearner-Pytorch

```
@article{luo2019every,
  title={Every pixel counts++: Joint learning of geometry and motion with 3d holistic understanding},
  author={Luo, Chenxu and Yang, Zhenheng and Wang, Peng and Wang, Yang and Xu, Wei and Nevatia, Ram and Yuille, Alan},
  journal={IEEE transactions on pattern analysis and machine intelligence},
  volume={42},
  number={10},
  pages={2624--2641},
  year={2019},
  publisher={IEEE}
}
```
