"""
Some utils functions

Tickets:
    - TODO: Add some tests
"""

import numpy as np
from PIL import Image

from torchvision import transforms

from matplotlib import pyplot as plt


def get_consecutive_tuples(input_list: list) -> list:
    """Returns the list of consecutive tuples in a list"""
    return [(x, input_list[i + 1]) for i, x in enumerate(input_list[:-1])]


def show_np_image(img: np.array) -> None:
    """Shows a [C, H, W] image"""
    pil_img = Image.fromarray(img.transpose(1, 2, 0))
    plt.imshow(pil_img)


def resize_like(inputs, ref):
    """
    Returns the Tensor "inputs" with the exact size of Tensor "ref".

    Tensors have shape [N, C, H, W]

    Taken from https://github.com/chenxuluo/EPC/blob/master/nets.py
    """
    iH, iW = inputs.shape[-2:]
    rH, rW = ref.shape[-2:]
    resize = transforms.Resize((rH, rW))
    if iH == rH and iW == rW:
        return inputs
    return resize(inputs)
