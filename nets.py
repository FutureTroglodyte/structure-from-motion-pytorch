"""
Some CNNs taken from https://github.com/chenxuluo/EPC/blob/master/nets.py

Tensorflow slim code was translated into pytorch

Tickets:
    - TODO: Make code more readable and elegant. Add Docstrings
    - TODO: Add and understand the explainability masks to PoseNet
    - TODO: Add PWCNet for optical flow estimation
        - https://github.com/NVlabs/PWC-Net
        - https://github.com/sniklaus/pytorch-pwc/blob/master/run.py
    - TODO: Split code into modules
"""

import logging

import torch
from torch import nn

from torchvision import transforms

from utils import resize_like


class DispNet(nn.Module):
    def __init__(self, disp_scaling, min_disp):
        super().__init__()
        self.disp_scaling = disp_scaling
        self.min_disp = min_disp

        # activation_functions
        self.relu = nn.ReLU()
        self.sigmoid = nn.Sigmoid()

        # encoder:
        self.cnv1 = nn.Conv2d(in_channels=3, out_channels=32, kernel_size=7, stride=2)
        self.cnv1b = nn.Conv2d(in_channels=32, out_channels=32, kernel_size=7, stride=1)

        self.cnv2 = nn.Conv2d(in_channels=32, out_channels=64, kernel_size=5, stride=2)
        self.cnv2b = nn.Conv2d(in_channels=64, out_channels=64, kernel_size=5, stride=1)

        self.cnv3 = nn.Conv2d(in_channels=64, out_channels=128, kernel_size=3, stride=2)
        self.cnv3b = nn.Conv2d(
            in_channels=128, out_channels=128, kernel_size=3, stride=1
        )

        self.cnv4 = nn.Conv2d(
            in_channels=128, out_channels=256, kernel_size=3, stride=2
        )
        self.cnv4b = nn.Conv2d(
            in_channels=256, out_channels=256, kernel_size=3, stride=1
        )

        self.cnv5 = nn.Conv2d(
            in_channels=256, out_channels=512, kernel_size=3, stride=2
        )
        self.cnv5b = nn.Conv2d(
            in_channels=512, out_channels=512, kernel_size=3, stride=1
        )

        self.cnv6 = nn.Conv2d(
            in_channels=512, out_channels=512, kernel_size=3, stride=2
        )
        self.cnv6b = nn.Conv2d(
            in_channels=512, out_channels=512, kernel_size=3, stride=1
        )

        self.cnv7 = nn.Conv2d(
            in_channels=512, out_channels=512, kernel_size=3, stride=2
        )
        self.cnv7b = nn.Conv2d(
            in_channels=512, out_channels=512, kernel_size=3, stride=1
        )

        # decoder:
        self.upcnv7 = nn.ConvTranspose2d(
            in_channels=512, out_channels=512, kernel_size=3, stride=2
        )
        self.icnv7 = nn.Conv2d(
            in_channels=1024, out_channels=512, kernel_size=3, stride=1
        )

        self.upcnv6 = nn.ConvTranspose2d(
            in_channels=512, out_channels=512, kernel_size=3, stride=2
        )
        self.icnv6 = nn.Conv2d(
            in_channels=1024, out_channels=512, kernel_size=3, stride=1
        )

        self.upcnv5 = nn.ConvTranspose2d(
            in_channels=512, out_channels=256, kernel_size=3, stride=2
        )
        self.icnv5 = nn.Conv2d(
            in_channels=512, out_channels=256, kernel_size=3, stride=1
        )

        self.upcnv4 = nn.ConvTranspose2d(
            in_channels=256, out_channels=128, kernel_size=3, stride=2
        )
        self.icnv4 = nn.Conv2d(
            in_channels=256, out_channels=128, kernel_size=3, stride=1
        )
        self.disp4 = nn.Conv2d(in_channels=128, out_channels=1, kernel_size=3, stride=1)

        self.upcnv3 = nn.ConvTranspose2d(
            in_channels=128, out_channels=64, kernel_size=3, stride=2
        )
        self.icnv3 = nn.Conv2d(
            in_channels=128 + 1, out_channels=64, kernel_size=3, stride=1
        )
        self.disp3 = nn.Conv2d(in_channels=64, out_channels=1, kernel_size=3, stride=1)

        self.upcnv2 = nn.ConvTranspose2d(
            in_channels=64, out_channels=32, kernel_size=3, stride=2
        )
        self.icnv2 = nn.Conv2d(
            in_channels=64 + 1, out_channels=32, kernel_size=3, stride=1
        )
        self.disp2 = nn.Conv2d(in_channels=32, out_channels=1, kernel_size=3, stride=1)

        self.upcnv1 = nn.ConvTranspose2d(
            in_channels=32, out_channels=16, kernel_size=3, stride=2
        )
        self.icnv1 = nn.Conv2d(
            in_channels=16 + 1, out_channels=16, kernel_size=3, stride=1
        )
        self.disp1 = nn.Conv2d(in_channels=16, out_channels=1, kernel_size=3, stride=1)

    def forward(self, x):
        """
        Forward Pass

        TODO: What About BN?
        """
        H = x.shape[2]
        W = x.shape[3]

        # encoder
        x = self.relu(self.cnv1(x))
        x = x1 = self.relu(self.cnv1b(x))
        x = self.relu(self.cnv2(x))
        x = x2 = self.relu(self.cnv2b(x))
        x = self.relu(self.cnv3(x))
        x = x3 = self.relu(self.cnv3b(x))
        x = self.relu(self.cnv4(x))
        x = x4 = self.relu(self.cnv4b(x))
        x = self.relu(self.cnv5(x))
        x = x5 = self.relu(self.cnv5b(x))
        x = self.relu(self.cnv6(x))
        x = x6 = self.relu(self.cnv6b(x))
        x = self.relu(self.cnv7(x))
        x = self.relu(self.cnv7b(x))

        # decoder:
        x = self.relu(self.upcnv7(x))
        x = resize_like(x, x6)
        x = torch.concat((x, x6), dim=1)
        x = self.relu(self.icnv7(x))

        x = self.relu(self.upcnv6(x))
        x = resize_like(x, x5)
        x = torch.concat((x, x5), dim=1)
        x = self.relu(self.icnv6(x))

        x = self.relu(self.upcnv5(x))
        x = resize_like(x, x4)
        x = torch.concat((x, x4), dim=1)
        x = self.relu(self.icnv5(x))

        x = self.relu(self.upcnv4(x))
        x = resize_like(x, x3)
        x = torch.concat((x, x3), dim=1)
        x = self.relu(self.icnv4(x))

        d4 = self.disp_scaling * self.sigmoid(self.disp4(x)) + self.min_disp
        d4_up = transforms.Resize((int(H / 4), int(W / 4)))(d4)

        x = self.relu(self.upcnv3(x))
        x = resize_like(x, d4_up)
        x2 = resize_like(x2, d4_up)
        x = torch.concat((x, x2, d4_up), dim=1)
        x = self.relu(self.icnv3(x))

        d3 = self.disp_scaling * self.sigmoid(self.disp3(x)) + self.min_disp
        d3_up = transforms.Resize((int(H / 2), int(W / 2)))(d3)

        x = self.relu(self.upcnv2(x))
        x = resize_like(x, d3_up)
        x1 = resize_like(x1, d3_up)
        x = torch.concat((x, x1, d3_up), dim=1)
        x = self.relu(self.icnv2(x))

        d2 = self.disp_scaling * self.sigmoid(self.disp2(x)) + self.min_disp
        d2_up = transforms.Resize((H, W))(d2)

        x = self.relu(self.upcnv1(x))
        x = resize_like(x, d2_up)
        x = torch.concat((x, d2_up), dim=1)
        x = self.relu(self.icnv1(x))

        d1 = self.disp_scaling * self.sigmoid(self.disp1(x)) + self.min_disp
        d1_up = transforms.Resize((H, W))(d1)

        return [d1_up, d2_up, d3_up, d4_up]


class PoseNet(nn.Module):
    def __init__(self, do_exp=True):
        super().__init__()
        # activation functions
        self.relu = nn.ReLU()
        # cnv1 to cnv5b are shared between pose and explainability prediction
        self.cnv1 = nn.Conv2d(in_channels=6, out_channels=16, kernel_size=7, stride=2)
        self.cnv2 = nn.Conv2d(in_channels=16, out_channels=32, kernel_size=5, stride=2)
        self.cnv3 = nn.Conv2d(in_channels=32, out_channels=64, kernel_size=3, stride=2)
        self.cnv4 = nn.Conv2d(in_channels=64, out_channels=128, kernel_size=3, stride=2)
        self.cnv5 = nn.Conv2d(
            in_channels=128, out_channels=256, kernel_size=3, stride=2
        )
        # Pose specific layers
        self.cnv6 = nn.Conv2d(
            in_channels=256, out_channels=256, kernel_size=3, stride=2
        )
        self.cnv7 = nn.Conv2d(
            in_channels=256, out_channels=256, kernel_size=3, stride=2
        )
        self.pose_pred = nn.Conv2d(
            in_channels=256, out_channels=6, kernel_size=1, stride=1
        )  # out_channels=6*num_source, activation_fn=None

    def forward(self, source_img, target_img):
        x = torch.concat((source_img, target_img), dim=1)
        x = self.relu(self.cnv1(x))
        x = self.relu(self.cnv2(x))
        x = self.relu(self.cnv3(x))
        x = self.relu(self.cnv4(x))
        x = self.relu(self.cnv5(x))
        x = self.relu(self.cnv6(x))
        x = self.relu(self.cnv7(x))
        x = self.pose_pred(x)
        x = torch.mean(x, dim=[2, 3])
        # Empirically we found that scaling by a small constant facilitates training.
        x = 0.01 * x

        return x


if __name__ == "__main__":
    logging.basicConfig(format="%(asctime)s - %(message)s", level=logging.INFO)

    DEVICE = "cuda" if torch.cuda.is_available() else "cpu"
    DISP_SCALING = 10
    MIN_DISP = 0.01

    disphnet = DispNet(disp_scaling=DISP_SCALING, min_disp=MIN_DISP).to(DEVICE)
    posenet = PoseNet(do_exp=False).to(DEVICE)

    logging.info(disphnet)
    logging.info(posenet)
