"""
Collection of loss functions

Tickets:
    - TODO: Add more loss functions
"""


def photometric_loss(target_img, source_img_warped, valid_points):
    """
    L1 Loss function

    The shape of valid_points.unsqueeze(1) is [B, 1, H, W]. It has values in {0, 1}
    """
    diff = (target_img - source_img_warped) * valid_points.unsqueeze(1).float()
    loss = diff.abs().mean()
    return loss
