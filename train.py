"""
Train the EPC (here just SfmLearner) Framework and save it to disk.

Tickets:
    - TODO: Add real camera intrinsics instead of dummy intrinsics
    - TODO: Add more losses
    - TODO: Add the explaination vs disp masks levels
"""

import logging

import os

import torch
from torch import nn
from torch.utils.data import DataLoader

from torchvision import transforms

from datasets import Kitti2015ImagePairs
from nets import DispNet, PoseNet
from inverse_warp import inverse_warp
from losses import photometric_loss


### Set Hyperparams ###

RAW_DATA_PATH = "data/KITTI_2015/raw_data"
OUTPUT_PATH = "models/model.pt"
DEVICE = "cuda" if torch.cuda.is_available() else "cpu"
BATCH_SIZE = 2
NUM_WORKERS = os.cpu_count()
EPOCHS = 1
LEARNING_RATE = 0.0001
DISP_SCALING = 10  # For DispNet
MIN_DISP = 0.01  # For DispNet
DUMMY_INTRINSICS = (
    torch.eye(3).reshape((1, 3, 3)).repeat(BATCH_SIZE, 1, 1).to(DEVICE)
)  # [B,3,3]


def train_loop(
    train_dataloader, disp_net, pose_net, intrinsics, optimizer, loss_func, epoch
):
    """
    Much inspiration from https://github.com/ClementPinard/SfmLearner-Pytorch
    """
    size = len(train_dataloader.dataset)

    for batch, Imgages in enumerate(train_dataloader):
        source_img_batch, target_img_batch = Imgages[0].to(DEVICE), Imgages[1].to(
            DEVICE
        )

        # Estimate depth map
        disparities_t = disp_net(target_img_batch)  # disp_1, ... , disp_4
        depths_t = [1 / disp for disp in disparities_t]
        depth_t = depths_t[0]  # [B, 1, H, W]
        depth_t = depth_t.squeeze()  # [B, H, W]

        # Estimate pose
        pose_vec_s_t = pose_net(target_img_batch, source_img_batch)  # [B, 6]

        # Dummy intrinsics

        # source_img_warped_batch is [B, 3, H, W]
        # valid_points is [B, H, W]
        source_img_warped_batch, valid_points = inverse_warp(
            img=source_img_batch,
            depth=depth_t,
            pose=pose_vec_s_t,
            intrinsics=intrinsics,
            rotation_mode="euler",
            padding_mode="zeros",
        )

        loss = loss_func(
            target_img=target_img_batch,
            source_img_warped=source_img_warped_batch,
            valid_points=valid_points,
        )

        # compute gradient and do backward step
        optimizer.zero_grad()
        loss.backward()
        optimizer.step()

        if batch % 25 == 0:
            loss, current = loss.item(), batch * BATCH_SIZE
            logging.info(
                f"loss: {loss:>7f}  [epoch: {epoch} - {current:>5d}/{size:>5d}]"
            )

    return None


def main():
    """Train the framework and save it to disk"""

    ### Display Hyperparams ###
    logging.info(
        f"""
        Hyperparams:
        - RAW_DATA_PATH: {RAW_DATA_PATH}
        - OUTPUT_PATH = {OUTPUT_PATH}
        - DEVICE: {DEVICE}
        - BATCHSIZE: {BATCH_SIZE}
        - NUM_WORKERS: {NUM_WORKERS}
        - EPOCHS: {EPOCHS}
        - LEARNING_RATE: {LEARNING_RATE}
        - DISP_SCALING: {DISP_SCALING}
        - MIN_DISP: {MIN_DISP}
        - DUMMY_INTRINSICS: \n{DUMMY_INTRINSICS}
        """
    )

    ### DataLoader ###

    data_transforms = transforms.Compose(
        [
            transforms.Resize(size=(1024, 1024)),
            # transforms.RandomHorizontalFlip(p=0.5),
            transforms.ToTensor(),
        ]
    )
    kitti_data = Kitti2015ImagePairs(
        target_dir=RAW_DATA_PATH, img_num="02", transform=data_transforms
    )
    kitti_dataloader = DataLoader(
        dataset=kitti_data,
        batch_size=BATCH_SIZE,
        num_workers=NUM_WORKERS,
        shuffle=True,
        drop_last=True,
    )
    logging.info(f"Number of raw data samples: {len(kitti_data)}")

    ### CNNs ###

    disp_net = nn.DataParallel(
        DispNet(disp_scaling=DISP_SCALING, min_disp=MIN_DISP)
    ).to(DEVICE)
    pose_net = nn.DataParallel(PoseNet(do_exp=False)).to(DEVICE)

    ### Optimizer ###

    adam = torch.optim.Adam(
        [{"params": disp_net.parameters()}, {"params": pose_net.parameters()}],
        lr=LEARNING_RATE,
    )

    ### Train the framework ###

    for epoch in range(EPOCHS):
        train_loop(
            train_dataloader=kitti_dataloader,
            disp_net=disp_net,
            pose_net=pose_net,
            intrinsics=DUMMY_INTRINSICS,
            optimizer=adam,
            loss_func=photometric_loss,
            epoch=epoch,
        )

    ### Save trained framework to disk ###

    torch.save(
        {
            "disp_state_dict": disp_net.state_dict(),
            "pose_state_dict": pose_net.state_dict(),
        },
        OUTPUT_PATH,
    )
    logging.info(f"Saved PyTorch Model State to {OUTPUT_PATH}")


if __name__ == "__main__":
    logging.basicConfig(format="%(asctime)s - %(message)s", level=logging.INFO)
    main()
